﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositoryPattern.Repositories;
using RepositoryPattern.Models;

namespace RepositoryPattern.Controllers
{
    public class CustomerController
    {
        public void ReadAllCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        public void ReadCustomerById(ICustomerRepository repository)
        {
            Console.WriteLine("Write customer ID:");
            int id = Convert.ToInt32(Console.ReadLine());
            PrintCustomer(repository.GetCustomer(id));
        }

        public void ReadCustomerByName(ICustomerRepository repository)
        {
            Console.WriteLine("Write customer name:");
            string name = Console.ReadLine();
            PrintCustomer(repository.GetCustomerByName(name));
        }

        public void ReadPageOfCustomers(ICustomerRepository repository)
        {
            Console.WriteLine("Write offset:");
            int limit = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Write limit");
            int offset = Convert.ToInt32(Console.ReadLine());
            PrintCustomers(repository.GetPageOfCustomers(limit, offset));
        }

        public void AddNewCustomer(ICustomerRepository repository)
        {
            Console.WriteLine("Write customer name:");
            string name = Console.ReadLine();
            Console.WriteLine("Write customer last name:");
            string lastName = Console.ReadLine();
            Console.WriteLine("Write customer country:");
            string country = Console.ReadLine();
            Console.WriteLine("Write customer postal code:");
            string postalCode = Console.ReadLine();
            Console.WriteLine("Write customer phone number:");
            string phoneNumber = Console.ReadLine();
            Console.WriteLine("Write customer email:");
            string email = Console.ReadLine();
            Customer c = new Customer()
            {
                CustomerName = name,
                CustomerLastName = lastName,
                CustomerCountry = country,
                CustomerPostalCode = postalCode,
                CustomerPhoneNumber = phoneNumber,
                CustomerEmail = email
            };
            if (repository.AddNewCustomer(c))
                Console.WriteLine("Success.");
            else
                Console.WriteLine("Failed.");
        }

        public void UpdateCustomer(ICustomerRepository repository)
        {
            Console.WriteLine("Write customer id:");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Write customer name:");
            string name = Console.ReadLine();
            Console.WriteLine("Write customer last name:");
            string lastName = Console.ReadLine();
            Console.WriteLine("Write customer country:");
            string country = Console.ReadLine();
            Console.WriteLine("Write customer postal code:");
            string postalCode = Console.ReadLine();
            Console.WriteLine("Write customer phone number:");
            string phoneNumber = Console.ReadLine();
            Console.WriteLine("Write customer email:");
            string email = Console.ReadLine();
            Customer c = new Customer()
            {
                CustomerId = id,
                CustomerName = name,
                CustomerLastName = lastName,
                CustomerCountry = country,
                CustomerPostalCode = postalCode,
                CustomerPhoneNumber = phoneNumber,
                CustomerEmail = email
            };
            if (repository.UpdateCustomer(c))
                Console.WriteLine("Success.");
            else
                Console.WriteLine("Failed.");
        }

        public void ReadCustomerByCountry(ICustomerRepository repository)
        {
            PrintCountries(repository.GetCustomersPerCountry());
        }

        public void ReadHighestSpendingCustomers(ICustomerRepository repository)
        {
            PrintHighestSpenders(repository.GetHighestSpenders());
        }

        public void ReadCustomerFavoriteGenre(ICustomerRepository repository)
        {
            Console.WriteLine("Write customer id:");
            int id = Convert.ToInt32(Console.ReadLine());
            PrintCustomerAndFavoriteGenre(repository.GetCustomersMostPopularGenre(id));
        }

        public void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer c in customers)
            {
                PrintCustomer(c);
            }
        }

        public void PrintCustomer(Customer c)
        {
            Console.WriteLine($"--- {c.CustomerId} {c.CustomerName} {c.CustomerLastName} {c.CustomerCountry} {c.CustomerPostalCode} {c.CustomerPhoneNumber} {c.CustomerEmail}");
        }

        public void PrintCountries(IEnumerable<CustomerCountry> contries)
        {
            foreach (CustomerCountry c in contries)
                Console.WriteLine($"--- {c.CountryName} {c.CountryCustomers}");
        }

        public void PrintHighestSpenders(IEnumerable<CustomerSpender> spenders)
        {
            foreach (CustomerSpender c in spenders)
                Console.WriteLine($"--- {c.CustomerSpenderName} {c.CustomerTotal}");
        }

        public void PrintCustomerAndFavoriteGenre(List<CustomerGenre> genre)
        {
            foreach (CustomerGenre c in genre)
                Console.WriteLine($"--- {c.CustomerName} {c.CustomerGenreName} ");
        }
    }
}
