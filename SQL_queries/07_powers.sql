INSERT INTO heropower(power_name, power_description)
VALUES ('Healing factor,', 'Regenerates damaged or destroyed tissues of the body far beyond that of normal humans. Healing factor makes the user extraordinarily resistant to diseases, drugs and toxins.');

INSERT INTO heropower (power_name, power_description)
VALUES ('Astral projection', 'The departure of the astral body from the physical body, in order to travel to the astral plane.');

INSERT INTO heropower (power_name, power_description)
VALUES ('Power armor', 'Gives superhuman strength and durability, flight, and an array of weapons.');

INSERT INTO heropower (power_name, power_description)
VALUES ('Psychokinesis', 'Allowing the user to influence a physical system without physical interaction.');

GO

INSERT INTO superheropowerrelation(superhero_id, heropower_id)
VALUES (1, 1);

INSERT INTO superheropowerrelation(superhero_id, heropower_id)
VALUES (1, 2);

INSERT INTO superheropowerrelation(superhero_id, heropower_id)
VALUES (1, 3);

INSERT INTO superheropowerrelation(superhero_id, heropower_id)
VALUES (2, 4);

INSERT INTO superheropowerrelation(superhero_id, heropower_id)
VALUES (3, 4);