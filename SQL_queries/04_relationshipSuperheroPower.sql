USE SuperheroesDb;

CREATE TABLE superheropowerrelation(
	superhero_id INT NOT NULL FOREIGN KEY REFERENCES superhero(superhero_id),
	heropower_id INT NOT NULL FOREIGN KEY REFERENCES heropower(power_id),
	PRIMARY KEY(superhero_id, heropower_id)
);